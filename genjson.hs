import System.Random
import Data.Char

main :: IO ()
main = do
    stdGen <- newStdGen
    let randInts = randoms stdGen :: [Int]
    putStrLn $ "'" ++ genJson randInts ++ "'"

genJson :: [Int] -> String
genJson randInts =
    genElement randInts

genElement :: [Int] -> String
genElement (randInt0 : randInt1 : randInts) =
    (genWhitespace randInt0)
        ++ (genValue randInts)
        ++ (genWhitespace randInt1)

genElements :: [Int] -> String
genElements (randInt : randInts) =
    case (mod randInt 2) of
        0 -> genElement randInts
        1 -> (genElement randInts) ++ "," ++ (genElement randInts)  -- TODO split rands

genWhitespace :: Int -> String
genWhitespace randInt =
    case (mod randInt 3) of
        0 -> "\n"
        1 -> " "
        2 -> ""

genValue :: [Int] -> String
genValue (randInt : randInts) =
    case (mod randInt 7) of 
        0 -> genObject randInts
        1 -> genArray randInts
        2 -> genString randInts
        3 -> genNumber $ head randInts
        4 -> genTrue
        5 -> genFalse
        6 -> genNull

genObject :: [Int] -> String
genObject (randInt : randInts) =
    case (mod randInt 2) of
        0 -> "{" ++ (genWhitespace $ head randInts) ++ "}"
        1 -> "{" ++ (genMembers randInts) ++ "}"

genArray :: [Int] -> String
genArray (randInt : randInts) =
    case (mod randInt 2) of
        0 -> "[" ++ (genWhitespace $ head randInts) ++ "]"
        1 -> "[" ++ (genElements randInts) ++ "]"

genString :: [Int] -> String
genString (randInt : randInts) =
    "\"" ++ (map intToAscii someRandInts) ++ "\""
    where
        someRandInts = take (1 + mod randInt 10) randInts  -- TODO magic numbers
        intToAscii i = chr (65 + mod i 26)  -- TODO magic numbers

genNumber :: Int -> String
genNumber randInt =
    show randInt  -- TODO more sophisticated

genTrue :: String
genTrue =
    "true"

genFalse :: String
genFalse =
    "false"

genNull :: String
genNull =
    "null"

genMembers :: [Int] -> String
genMembers (randInt : randInts) =
    case (mod randInt 2) of
        0 -> (genMember randInts)
        1 -> (genMember randInts) ++ "," ++ (genMembers randInts)  -- TODO split rands

genMember :: [Int] -> String
genMember (randInt0 : randInt1 : randInts) =
    (genWhitespace randInt0)
        ++ (genString randInts)
        ++ (genWhitespace randInt1)
        ++ ":"
        ++ (genElement randInts)
        -- TODO split rands
